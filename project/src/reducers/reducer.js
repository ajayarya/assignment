
const initialState = {
    loading: false,
    restaurantList: [],
    error: null,
    isLoggedIn: false
}

export const restaurantReducer = (state = initialState, action) => {
    switch (action.type) {
        case ("FETCH_RESTAURANT_REQUEST"):
            return Object.assign({}, state, { loading: true, error: null });

        case ("FETCH_RESTAURANT_SUCCESS"):
            return Object.assign({}, state, { restaurantList: action.payload, loading: false });

        case ("FETCH_RESTAURANT_FAILURE"):
            return Object.assign({}, state, { loading: false, error: action.error });
        case ("LOGIN"):
            return Object.assign({}, state, { isLoggedIn: true });
        case ("LOGOUT"):
            return Object.assign({}, state, { isLoggedIn: false });
        default:
            return state;
    };
}