import axios from "axios";

export const fetchRestaurants = (searchBy = "", field = "name", orderBy = "asc") => {
    return async (dispatch, getState) => {
        dispatch({
            type: "FETCH_RESTAURANT_REQUEST"
        });

        try {
            const response = await axios.get(`http://localhost:3000/restaurants?q=${searchBy}&_sort=${field}&_order=${orderBy}`);
            console.log(response);

            dispatch({
                type: "FETCH_RESTAURANT_SUCCESS",
                payload: response.data
            })
        }
        catch (error) {
            dispatch({
                type: "FETCH_RESTAURANT_FAILURE",
                error
            })
        }
    }
}