import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import RestaurantCreate from './components/RestaurantCreate';

import RestaurantList from './components/RestaurantList';
import Login from './components/Login';
import NavbarMenu from './components/NavBarMenu';
import Logout from './components/Logout';
import SignUp from './components/signUp';

function App() {
  return (
    <div className="App">

      <Router>
        <NavbarMenu />
        <Route path="/list" component={RestaurantList} />

        <Route path="/create" component={RestaurantCreate} />

        <Route path="/login" component={Login} />

        <Route path="/logout" component={Logout} />

        <Route path="/signup" component={SignUp} />

        <Route exact path="/" component={RestaurantList} />
      </Router>
    </div >
  );
}

export default App;
