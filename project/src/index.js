import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { restaurantReducer } from './reducers/reducer';


const rootReducer = combineReducers({ restaurantReducer })

const store = createStore(rootReducer, applyMiddleware(thunk));

//  Commit from assignment folder

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'))
