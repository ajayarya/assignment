import React from 'react';
import {
    Redirect
} from 'react-router-dom';

const Logout = () => {

    // Clearing localStorage which was saved at the time of login and then redirecting to login screen

    localStorage.clear();

    return (
        <Redirect to="/login" />
    )
}

export default Logout;