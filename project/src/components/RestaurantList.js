import React, { Component } from 'react';
import { Table, Form, FormControl, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortDown, faSortUp, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { connect } from "react-redux";
import { fetchRestaurants } from '../actions/action';


class RestaurantList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: null,
            orderBy: "asc",
            orderByField: "name",
            searchBy: ""
        }
        this.getSortedList = this.getSortedList.bind(this);
    }

    componentDidMount() {
        this.props.fetchRestaurants();
    }

    deleteRestaurant(id) {
        fetch("http://localhost:3000/restaurants/" + id, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }).then((response) => response.json())
            .then(data => {
                alert("Restaurant has been deleted");
                this.props.fetchRestaurants(this.state.searchBy, this.state.orderByField, this.state.orderBy);
            })
    }

    getSortedList() {
        this.props.fetchRestaurants(this.state.searchBy, this.state.orderByField, this.state.orderBy);
    }

    sort(field) {
        this.setState({
            orderByField: field,
            orderBy: this.state.orderBy === "asc" ? "desc" : "asc"
        }, this.getSortedList())

    }

    render() {

        const { restaurantList, loading } = this.props.restaurantReducer;


        return (
            <div className="container">
                {

                    <div className="marginTopBottm">
                        {loading ? <h1 className="headingText">Please wait...</h1> :
                            <div>
                                <h1 className="headingText">Restaurant List</h1>

                                <Form inline onSubmit={(e) => { e.preventDefault() }} style={{ "marginBottom": "20px" }}>
                                    <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={(e) => this.setState({ searchBy: e.target.value })} />
                                    <Button variant="outline-success" type="button" onClick={() => { this.props.fetchRestaurants(this.state.searchBy) }}>Search</Button>
                                </Form>

                                <Table striped bordered hover>
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th onClick={() => this.sort('name')} style={{ "cursor": "Pointer" }}>
                                                Restaurant Name <span>{this.state.orderByField === "name" ? <FontAwesomeIcon icon={this.state.orderBy === "asc" ? faSortUp : faSortDown} style={{ "cursor": "Pointer" }} /> : null}</span>
                                            </th>
                                            <th onClick={() => this.sort('email')} style={{ "cursor": "Pointer" }}>
                                                Owner Email <span>{this.state.orderByField === "email" ? <FontAwesomeIcon icon={this.state.orderBy === "asc" ? faSortUp : faSortDown} style={{ "cursor": "Pointer" }} /> : null}</span>
                                            </th>
                                            <th onClick={() => this.sort('rating')} style={{ "cursor": "Pointer" }}>
                                                Restaurant Rating <span>{this.state.orderByField === "rating" ? <FontAwesomeIcon icon={this.state.orderBy === "asc" ? faSortUp : faSortDown} style={{ "cursor": "Pointer" }} /> : null}</span>
                                            </th>
                                            <th onClick={() => this.sort('address')} style={{ "cursor": "Pointer" }}>
                                                Restaurant Address <span>{this.state.orderByField === "address" ? <FontAwesomeIcon icon={this.state.orderBy === "asc" ? faSortUp : faSortDown} style={{ "cursor": "Pointer" }} /> : null}</span>
                                            </th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {restaurantList.map((item, i) => {
                                            console.log(item);
                                            return <tr key={item.id}>
                                                <td>{i + 1}</td>
                                                <td>{item.name}</td>
                                                <td>{item.email}</td>
                                                <td>{item.rating}</td>
                                                <td>{item.address}</td>
                                                <td className="text-center"><FontAwesomeIcon icon={faTrashAlt} color={"red"} style={{ "cursor": "Pointer" }} onClick={() => this.deleteRestaurant(item.id)} /></td>
                                            </tr>
                                        })}


                                    </tbody>
                                </Table>
                            </div >
                        }

                    </div>

                }
            </div>
        );
    }
}

const mapStateToProps = ({ restaurantReducer }) => {
    return {
        restaurantReducer
    };
};

export default connect(mapStateToProps, {
    fetchRestaurants
})(RestaurantList);