import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            password: ""
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    componentDidMount() {
        this.props.dispatch({ type: "LOGOUT" })
    }

    // Login method for calling login fake api (it is just searching based on name only not password)

    login() {
        fetch("http://localhost:3000/login?q=" + this.state.name)
            .then((response) => response.json())
            .then(data => {
                if (data.length > 0) {
                    this.props.dispatch({ type: "LOGIN" })
                    this.props.history.push('/list');
                    localStorage.setItem('login', JSON.stringify(data))
                }
                else {
                    alert('Please check username and password')
                }
            })
    }

    render() {

        return (
            <div className="container col-6 text-left">
                <div className={"center-block marginTopBottm"}>
                    <Form>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" name="name" placeholder="Name" onChange={(e) => this.handleChange(e)} />
                        </Form.Group>

                        <Form.Group >
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" placeholder="Password" onChange={(e) => this.handleChange(e)} />
                        </Form.Group>

                        <Button variant="primary" onClick={() => this.login()}>
                            Submit
                    </Button>
                    </Form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ restaurantReducer }) => {
    return {
        restaurantReducer
    };
};


export default connect(mapStateToProps, null)(Login);