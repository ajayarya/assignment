import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';

class RestaurantCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            rating: "",
            address: ""
        }
    }

    handleChange(e) {
        console.log(e);
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    // Submit fake api to create restaurants

    submit() {
        fetch("http://localhost:3000/restaurants", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then((response) => response.json())
            .then(data => {
                alert("Restaurant has been added");
                this.props.history.push('/list')
            })
    }

    render() {
        console.log(this.state);
        console.log(this.props);
        return (
            <div className="container col-sm-6 text-left">
                <Form>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Restaurant Name</Form.Label>
                        <Form.Control type="text" name="name" placeholder="Restaurant Name" onChange={(e) => this.handleChange(e)} />
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Restaurant Email</Form.Label>
                        <Form.Control type="email" name="email" placeholder="Restaurant Email" onChange={(e) => this.handleChange(e)} />
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Restaurant Rating</Form.Label>
                        <Form.Control type="text" name="rating" placeholder="Restaurant Rating" onChange={(e) => this.handleChange(e)} />
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Restaurant Address</Form.Label>
                        <Form.Control type="text" name="address" placeholder="Restaurant Address" onChange={(e) => this.handleChange(e)} />
                    </Form.Group>
                    <Button variant="primary" onClick={() => this.submit()}>
                        Submit
                    </Button>
                </Form>
            </div>
        );
    }
}

export default RestaurantCreate;