import React, { Component } from 'react';
import { Navbar, Nav, FormControl, Button, Form } from 'react-bootstrap';
import {
    Link
} from 'react-router-dom';
import { fetchRestaurants } from '../actions/action';
import { connect } from "react-redux";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faPlus, faUser } from '@fortawesome/free-solid-svg-icons';

class NavbarMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {

        // Taking out the login object saved in the localStorage

        const userLoggedIn = localStorage.getItem("login");

        return (
            <div className="container">
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand href="#home">Restaurants</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">

                            <Nav.Link><Link to="/list"><FontAwesomeIcon icon={faList} /> Listing</Link></Nav.Link>
                            {
                                userLoggedIn ? <Nav.Link><Link to="/create"><FontAwesomeIcon icon={faPlus} /> Create</Link></Nav.Link> : null
                            }

                            {
                                userLoggedIn ? null : <Nav.Link><Link to="/signup"><FontAwesomeIcon icon={faUser} /> Sign Up</Link></Nav.Link>
                            }

                            {
                                userLoggedIn ?
                                    <Nav.Link><Link to="/logout"><FontAwesomeIcon icon={faUser} /> Logout</Link></Nav.Link> :
                                    <Nav.Link><Link to={{
                                        pathname: "/login"
                                    }}><FontAwesomeIcon icon={faUser} /> Login</Link></Nav.Link>
                            }

                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}

const mapStateToProps = ({ restaurantReducer }) => {
    return {
        restaurantReducer
    };
};


export default connect(mapStateToProps, {
    fetchRestaurants
})(NavbarMenu);