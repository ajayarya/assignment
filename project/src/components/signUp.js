import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            password: ""
        }
    }

    handleChange(e) {
        console.log(e);
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    // Sign Up fake api to create user

    signUp() {
        fetch("http://localhost:3000/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then((response) => response.json())
            .then(data => {
                alert("You are registered successfully");
                this.props.history.push('/login')
            })
    }

    render() {

        console.log(JSON.stringify(this.state));

        return (
            <div className="container col-6 text-left">
                <div className={"center-block marginTopBottm"}>
                    <Form>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" name="name" placeholder="Name" onChange={(e) => this.handleChange(e)} />
                        </Form.Group>

                        <Form.Group >
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" placeholder="Password" onChange={(e) => this.handleChange(e)} />
                        </Form.Group>

                        <Button variant="primary" onClick={() => this.signUp()}>
                            Submit
                    </Button>
                    </Form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ restaurantReducer }) => {
    return {
        restaurantReducer
    };
};


export default connect(mapStateToProps, null)(Login);